Instructions:
1. Verify and set environment variables in env file '.env'. There is a file called 'example.env' for reference.
2. Build and start up the application: docker-compose up --build
3. Verify status of services: docker-compose ps
