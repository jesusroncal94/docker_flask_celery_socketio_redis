import os
import random
import time
import uuid
from celery import Celery
from distutils.util import strtobool
from flask import Flask, jsonify, redirect, render_template, request, session, url_for
from flask_mail import Mail, Message
from flask_socketio import join_room, leave_room, SocketIO
from random import randint


# App configuration
app = Flask(__name__)
app.config['SECRET_KEY'] = os.getenv('SECRET_KEY')

# Flask-Mail configuration
app.config['MAIL_SERVER'] = os.getenv('MAIL_SERVER')
app.config['MAIL_PORT'] = os.getenv('MAIL_PORT')
app.config['MAIL_USE_TLS'] = strtobool(os.getenv('MAIL_USE_TLS'))
app.config['MAIL_USE_SSL'] = strtobool(os.getenv('MAIL_USE_SSL'))
app.config['MAIL_USERNAME'] = os.getenv('MAIL_USERNAME')
app.config['MAIL_PASSWORD'] = os.getenv('MAIL_PASSWORD')
app.config['MAIL_DEFAULT_SENDER'] = os.getenv('MAIL_DEFAULT_SENDER')

# Celery configuration
app.config['CELERY_BROKER_URL'] = os.getenv('CELERY_BROKER_URL')

# SocketIO configuration
app.config['SOCKETIO_BROKER_URL'] = os.getenv('SOCKETIO_BROKER_URL')

# Initialize Mail
mail = Mail(app)

# Initialize Celery
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

# Initialize SocketIO
socketio = SocketIO(app, message_queue=app.config['SOCKETIO_BROKER_URL'])


# Custom function that uses the emit method to send messages to the client
def send_message(event, message, namespace, room):
    print(message)
    socketio.emit(event, {'msg': message}, namespace=namespace, room=room)


# Asynchronous task for counter
@celery.task()
def async_counter(n, session_id):
    # Room and namespace for counter
    room = session_id
    namespace = '/async_counter'

    # Send messages through the status and msg events for the start of the task
    send_message('status', 'Begin', namespace, room)
    send_message('msg', f'Begin task {async_counter.request.id}', namespace, room)
    send_message('msg', f'This task will take {n} seconds.', namespace, room)

    # Counter
    for i in range(1, n + 1):
        # Send message for each iteration
        send_message('msg', str(i), namespace, room)
        # Suspend iteration for 1 second
        time.sleep(1)

    # Send messages through the status and msg events for the end of the task
    send_message('msg', f'End task {async_counter.request.id}', namespace, room)
    send_message('status', 'End', namespace, room)


# Asynchronous task for sending email
@celery.task()
def async_send_email(email_data, session_id):
    room = session_id
    namespace = '/async_send_email'

    send_message('status', 'Begin', namespace, room)
    send_message('msg', f'Begin task {async_send_email.request.id}', namespace, room)
    
    # Instance and structure of the message
    msg = Message(
        subject=email_data['subject'],
        sender=('App (Bot)', app.config['MAIL_DEFAULT_SENDER']),
        recipients=[email_data['to']],
        body=email_data['body']
    )

    # Manual push of application context
    with app.app_context():
        send_message('msg', 'Sending...', namespace, room)
        # Send email
        mail.send(message=msg)

    send_message('msg', f'End task {async_send_email.request.id}', namespace, room)
    send_message('status', 'End', namespace, room)


# Asynchronous task for progress bar
@celery.task()
def async_progress_bar(session_id):
    room = session_id
    namespace = '/async_progress_bar'

    # Lists with verbs, adjectives, and nouns to put together a random sentence
    verb = ['Starting up', 'Booting', 'Repairing', 'Loading', 'Checking']
    adjective = ['master', 'radiant', 'silent', 'harmonic', 'fast']
    noun = ['solar array', 'particle reshaper', 'cosmic ray', 'orbiter', 'bit']
    # Random length for progress bar
    total = random.randint(5, 15)

    for i in range(0, total + 1):
        # Start the progress bar -> Begins at 0
        if(i == 0):
            state = 'INITIATED'
            status = f'{random.choice(verb)} {random.choice(adjective)} {random.choice(noun)}...'
        # Bar progress
        elif(i > 0 and i < total):
            state = 'PROGRESS'
            status = f'{random.choice(verb)} {random.choice(adjective)} {random.choice(noun)}...'
        # End the progress bar
        elif(i == total):
            state = 'COMPLETED'
            status = 'Task completed!'

        # Dictionary with the necessary data to draw the progress bar
        message = {
            'id': async_progress_bar.request.id,
            'state': state,
            'current': (i * 100) // total,
            'total': total,
            'status': status
        }
        time.sleep(1)
        send_message('msg', message, namespace, room)


# Initial route
@app.route("/", methods=['GET'])
def index():
    # Pass unique id for the session and use it later for the room
    if 'uuid' not in session:
        session['uuid'] = str(uuid.uuid4())

    return render_template('index.html')


# Route for the counter
@app.route("/counter", methods=['POST'])
def counter():
    n = randint(5, 10)
    # Get unique id of the session and pass it to the asynchronous task
    session_id = str(session['uuid'])
    # Start task
    task = async_counter.delay(n=n, session_id=session_id)
    
    # Return task id
    return jsonify({'id': task.id})


# Route for sending email
@app.route("/sendEmail", methods=['POST'])
def send_email():
    # Get email of the form and structure the data for sending
    email = request.form['email']
    email_data = {
        'subject': 'Hello from Flask with Celery',
        'to': email,
        'body': 'This is a test email sent from a background Celery task.'
    }
    session_id = str(session['uuid'])
    task = async_send_email.delay(email_data=email_data, session_id=session_id)

    return jsonify({'id': task.id})


# Route for progress bar
@app.route("/progressBar", methods=['POST'])
def progress_bar():
    session_id = str(session['uuid'])
    task = async_progress_bar.delay(session_id=session_id)

    return jsonify({'id': task.id})


# Connect with socket server
@socketio.on('connect')
def socket_connect():
    pass


# Custom function to connect with namespace async_counter and specific room
@socketio.on('join_async_counter', namespace='/async_counter')
def on_room():
    # Get unique id of session for room
    room = str(session['uuid'])
    print(f'join room {room}')
    # Join specific room
    join_room(room)


# Custom function to connect with namespace async_send_email and specific room
@socketio.on('join_async_send_email', namespace='/async_send_email')
def on_room():
    room = str(session['uuid'])
    print(f'join room {room}')
    join_room(room)


# Custom function to connect with namespace async_progress_bar and specific room
@socketio.on('join_async_progress_bar', namespace='/async_progress_bar')
def on_room():
    room = str(session['uuid'])
    print(f'join room {room}')
    join_room(room)


# Run app
if __name__ == "__main__":
    # Instead of running the application under the Flask instance, it runs through the SocketIO instance
    # It is necessary to pass as an argument to the Flask instance
    socketio.run(
        app=app,
        host=os.getenv('FLASK_RUN_HOST'),
        port=os.getenv('FLASK_RUN_PORT'),
        debug=strtobool(os.getenv('FLASK_DEBUG'))
    )
